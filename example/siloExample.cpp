#define EXAMPLE_WITH_LOGGING 1
#include <silo.hpp>

int main( void )
{

  example::Logger logger( "log", std::cerr, example::LOG_LEVEL_VERBOSE );
  logger.log( "Hello log!", example::LOG_LEVEL_VERBOSE, EXAMPLE_FILE_LINE );

  std::this_thread::sleep_for(std::chrono::seconds(1));
  logger.log( "Goodbye!", example::LOG_LEVEL_WARNING, EXAMPLE_FILE_LINE );

  std::this_thread::sleep_for(std::chrono::seconds(1));
  logger.log( "Error!", example::LOG_LEVEL_ERROR, EXAMPLE_FILE_LINE );

  std::this_thread::sleep_for(std::chrono::seconds(1));
  logger.log( "Critical!", example::LOG_LEVEL_CRITICAL, EXAMPLE_FILE_LINE );

  return 0;
}
