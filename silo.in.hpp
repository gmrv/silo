#ifndef __@SILO_NAME@_LOG__
#define __@SILO_NAME@_LOG__

#include <iostream>
#include <string>
#include <chrono>
#include <iomanip>
#include <unordered_map>
#include <thread>

namespace @SILO_NAMESPACE@
{

#define @SILO_NAME@_LOG_HEADER                                  \
  std::string( "@SILO_NAME@: " +                                \
               std::string( __FILE__ ) + std::string( "(" ) +   \
               std::to_string( __LINE__ ) +                     \
               std::string( ")::" ) +                           \
               std::string( __FUNCTION__ ) +                    \
               std::string( ": " ))

#define @SILO_NAME@_FILE_LINE                                    \
  std::string( __FILE__ ) + std::string( ":" ) +                 \
  std::to_string( __LINE__ )                                     \


  //! Log levels suported
  typedef enum
  {
    LOG_LEVEL_CRITICAL = 0,
    LOG_LEVEL_ERROR,
    LOG_LEVEL_WARNING,
    LOG_LEVEL_VERBOSE,
    LOG_LEVEL_UNKNOWN
  } TLogLevel;

  /**
   * class to log output
   *
   */
  class Logger
  {
  public:

    Logger( const std::string& logName_ = "",
            std::ostream& stream_ = std::cerr,
            TLogLevel logLevel_ =
#ifdef DEBUG
            LOG_LEVEL_WARNING
#else
            LOG_LEVEL_ERROR
#endif
            ,
            bool coloredOutput_ = true
      )
      : _logName( logName_ )
      , _startTime( std::chrono::system_clock::now( ))
      , _logLevel( logLevel_ )
      , _stream( &stream_ )
      , _coloredOutput( coloredOutput_ )
    {
      stream_ << "| Date                | "
              << "Uptime  | "
              << "LogName | "
              << "Lev | "
              << "Thread          | "
              << "File/Line                      | "
              << "Message "
              << std::endl;
    }

    void setCurrentThreadName( const std::string& threadName )
    {
      _threadNames[ std::this_thread::get_id( ) ] = threadName;
    }

    /**
     * Set current log level
     * @param level level to be set
     */
    void setLogLevel( const TLogLevel logLevel_ )
    {
      _logLevel = logLevel_;
    }

    /**
     * Get current log level
     * @return current log level
     */
    TLogLevel logLevel( void )
    {
      return _logLevel;
    };

    /**
     * Set current output stream
     * @param stream output stream to be set
     */
    void setStream( std::ostream& stream_ )
    {
      _stream = &stream_;
    }

    /**
     * Get current output stream
     * @return current output stream
     */
    const std::ostream& stream( void )
    {
      return *_stream;
    }

    /**
     * Method to log out a message in a specific level
     * @param msg message to log
     * @param level level of the message
     */
    void log( const std::string& msg,
              const TLogLevel level = LOG_LEVEL_VERBOSE,
              const std::string& fileAndLine = "" )
    {
#ifdef @SILO_NAME@_WITH_LOGGING
      this->log( msg, *_stream, level, fileAndLine );
#endif
    }

    /**
     * Method to log out a message in a specific level to a specific stream
     * @param msg message to log
     * @param level level of the message
     * @param stream stream where log will be written
     */
    void log( const std::string& msg,
              std::ostream& stream,
              const TLogLevel level = LOG_LEVEL_VERBOSE,
              const std::string& fileAndLine = "" )
    {
#ifdef @SILO_NAME@_WITH_LOGGING

      // Reset colors
      if ( _coloredOutput )
      {
        stream << "\x1b[0m";
        switch ( level )
        {
        case LOG_LEVEL_CRITICAL:
          stream << "\x1b[31;3m"; break;
        case LOG_LEVEL_ERROR:
          stream << "\x1b[31;1m"; break;
        case LOG_LEVEL_WARNING:
          stream << "\x1b[33;1m"; break;
        case LOG_LEVEL_VERBOSE:
          stream << "\x1b[34;1m"; break;
        default:
          stream << "\x1b[0m";
        }
      }

      TLogLevel currentLogLevel = _logLevel;

      char* envLogLevelString = getenv ( "@SILO_NAME@_LOG_LEVEL" );

      if ( envLogLevelString )
      {
        auto envLogLevel = stringToLogLevel( envLogLevelString );

        if ( envLogLevel != LOG_LEVEL_UNKNOWN )
          currentLogLevel = envLogLevel;
      }

      if ( currentLogLevel >= level )
      {
        auto now = std::chrono::system_clock::now( );
        auto nowTime = std::chrono::system_clock::to_time_t( now );
        stream << std::put_time( std::localtime( &nowTime ), "| %F %T | ");

        auto uptimeMiliSecs =
          std::chrono::duration_cast< std::chrono::milliseconds >(
            std::chrono::system_clock::now( ) - _startTime ).count( );
        auto uptimeSecs = uptimeMiliSecs * 0.001;
        stream << std::setfill( '0' ) << std::setw( 7 );
        stream << std::fixed;
        stream << std::setprecision( 3 );
        stream << uptimeSecs << " | ";

        stream <<  std::setfill( ' ' ) << std::setw ( 7 )
               << _logName.substr( 0, 7 ) << " | ";

        switch ( level )
        {
        case LOG_LEVEL_CRITICAL:
          stream <<  "CRI | "; break;
        case LOG_LEVEL_ERROR:
          stream <<  "ERR | "; break;
        case LOG_LEVEL_WARNING:
          stream <<  "WAR | "; break;
        case LOG_LEVEL_VERBOSE:
          stream <<  "VER | "; break;
        default:
          stream << " *** | ";
        }

        auto thisThreadId = std::this_thread::get_id( );
        auto thisThreadNameIt = _threadNames.find( thisThreadId );

        stream <<  std::setfill( ' ' ) << std::setw ( 15);
          if ( thisThreadNameIt == _threadNames.end( ))
          stream << thisThreadId << " | ";
        else
          stream << thisThreadNameIt->second.substr( 0, 15 ) << " | ";

        auto basename = fileAndLine;
        const auto lastSlashIdx = basename.find_last_of("\\/");
        if ( std::string::npos != lastSlashIdx )
          basename.erase( 0, lastSlashIdx + 1 );

        stream <<  std::setfill( ' ' ) << std::setw ( 30 );
        stream << basename.substr( 0, 30 ) << " | ";

        stream << std::boolalpha
               << msg << std::endl;

        // Reset colors
        if ( _coloredOutput )
          stream << "\x1b[0m";
      }


#endif
    }

  protected:

    TLogLevel stringToLogLevel( std::string logLevelString )
    {
      if ( logLevelString == "CRITICAL" )
        return LOG_LEVEL_CRITICAL;

      if ( logLevelString == "ERROR" )
        return LOG_LEVEL_ERROR;

      if ( logLevelString == "WARNING" )
        return LOG_LEVEL_WARNING;

      if ( logLevelString == "VERBOSE" )
        return LOG_LEVEL_VERBOSE;

      std::cerr << "silo: Unknown log level" << std::endl;
      return LOG_LEVEL_UNKNOWN;
    }

    std::string _logName;

    std::chrono::time_point<std::chrono::system_clock> _startTime;

    //! Current log level. This can be overwritten with proper value
    //! assigned to environment variable @SILO_NAME@_LOG_LEVEL
    TLogLevel _logLevel;

    //! Current stream to write log to
    std::ostream* _stream;

    std::unordered_map< std::thread::id, std::string> _threadNames;

    bool _coloredOutput;
  }; // class Log
} // namespace

#endif
